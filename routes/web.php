<?php

use App\Http\Controllers\Crowfunding\DonationController;
use App\Http\Controllers\Crowfunding\ProfileController;
use App\Http\Controllers\Crowfunding\UserController;
use App\Http\Controllers\Crowfunding\WelcomeController;
use App\Models\Kampanye;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [WelcomeController::class, 'index'])->name('welcome');

Route::get('profile', [ProfileController::class, 'index'])->name('profile');
Route::get('editProfile', [ProfileController::class, 'editProfile'])->name('editProfile.get');
Route::post('editProfile', [ProfileController::class, 'editProfileStore'])->name('editProfile.store');
Route::get('editPassword', [ProfileController::class, 'editPassword'])->name('editPassword.get');
Route::post('editPassword', [ProfileController::class, 'editPasswordStore'])->name('editPassword.store');
Route::post('addPhoto', [ProfileController::class, 'addPhoto'])->name('addPhoto.store');

Route::get('donation/list', [DonationController::class, 'index'])->name('donation.index');
Route::get('donation/detail/{id}', [DonationController::class, 'detail'])->name('donation.detail');
Route::get('donation/send', [DonationController::class, 'send'])->name('donation.send');
Route::get('donation/success', [DonationController::class, 'success'])->name('donation.success');
Route::get('donation/user/{id}', [DonationController::class, 'donate_user'])->name('donation.user');

Route::post('register', [UserController::class, 'register'])->name('user.register');
Route::post('login', [UserController::class, 'login'])->name('user.login');
Route::post('logout', [UserController::class, 'logout'])->name('user.logout');

Route::get('about', function (){
    return view('main.about');
})->name('about');

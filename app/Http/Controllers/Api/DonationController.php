<?php

namespace App\Http\Controllers\Api;

use App\Models\Kampanye;
use App\Traits\Myhelper;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;

class DonationController extends Controller
{
    use Myhelper;

    public function addCampaign()
    {
        $validation = Validator::make(request()->all(), [
            'title' => 'required',
            'desc' => 'required',
            'photo' => 'required',
            'target' => 'required',
            'deadline' => 'required',
        ]);

        if ($validation->fails()) return response()->json($validation->errors(), 422);

        try {
            if (request()->file('photo') !== null) {
                $image = base64_encode(file_get_contents(request()->file('photo')->path()));

                // Obtain the original content (usually binary data)
                $bin = base64_decode($image);
                $decoded = base64_decode($image, true);

                // image verify check
                if (!is_string($image) || false === $decoded) {
                    return response()->json([
                        'status' => false,
                        'msg' => 'invalid format image'
                    ], 422);
                }

                // Load GD resource from binary data
                $im = imageCreateFromString($bin);

                if (!$im) {
                    return response()->json([
                        'status' => false,
                        'msg' => 'file is not an image'
                    ], 406);
                }

                // Specify the location where you want to save the image
                $img_name = Str::random(6) . '-' . time() . '.png';
                if (is_dir(public_path('assets/image/campaign')) == false) {
                    mkdir(public_path('assets/image/campaign'));
                }
                $img_file = public_path('assets/image/campaign/' . $img_name);
                imagepng($im, $img_file, 0);
                $photo = url('assets/image/campaign/' . $img_name);
            }

            Kampanye::create([
                'judul' => request()->title,
                'deskripsi' => request()->desc,
                'gambar' => $photo,
                'target' => request()->target,
                'tenggat' => request()->deadline,
                'status' => 'active'
            ]);

            return response()->json([
                'status' => true,
                'msg' => 'success'
            ], 200);
        } catch (QueryException $error) {
            return $this->error_response($error);
        }
    }

    public function addDonation()
    {
        //
    }
}

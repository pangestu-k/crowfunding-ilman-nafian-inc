<?php

namespace App\Http\Controllers\Crowfunding;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Traits\Myhelper;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    use Myhelper;

    public function register()
    {
        $this->validate(request(), [
            'name' => 'required',
            'email' => 'required|unique:users,email',
            'gender' => 'required',
            'password' => 'required|confirmed|min:6',
        ]);

        try {
            User::create([
                'nama' => request()->name,
                'email' => request()->email,
                'gender' => request()->gender,
                'password' => Hash::make(request()->password),
                'role' => 'user'
            ]);

            Auth::attempt(request()->only('email', 'password'));
            return redirect()->route('profile');
        } catch (QueryException $errror) {
            return view('main.errror-page');
        }
    }

    public function login()
    {
        $this->validate(request(), [
            'email' => 'required',
        ]);

        try {
            $user = User::where('email', request()->email)->first();
            if ($user == null) {
                return back()->with('fail', ['email' => 'Your Email is not Register'])->with('email', request()->email);
            } else {
                if (Hash::check(request()->password, $user->password)) {
                    Auth::attempt(request()->only('email', 'password'));
                    return redirect()->route('profile');
                } else {
                    return back()->with('fail', ['password' => 'Input Password is Wrong'])->with('email', request()->email);
                }
            }
        } catch (QueryException $errror) {
            return view('main.errror-page');
        }
    }

    public function logout()
    {
        try {
            Auth::logout();
            return redirect()->route('profile');
        } catch (QueryException $errror) {
            return view('main.errror-page');
        }
    }
}

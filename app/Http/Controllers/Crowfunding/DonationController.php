<?php

namespace App\Http\Controllers\Crowfunding;

use App\Http\Controllers\Controller;
use App\Models\Donasi;
use Illuminate\Database\QueryException;

class DonationController extends Controller
{
   public function index()
   {
        return view('main.donation');
   }

   public function detail()
   {
        return view('main.detail-donate');
   }

   public function send()
   {
        return view('main.donation-send');
   }

   public function success()
   {
        return view('main.success-page');
   }

   public function donate_user($id){
       try {
            $donations = Donasi::with('campaign')->where('id_user', $id)->get();
            return view('main.donation-user', compact('donations'));
        } catch (QueryException $error) {
            return view('main.errror-page');
        }
   }
}

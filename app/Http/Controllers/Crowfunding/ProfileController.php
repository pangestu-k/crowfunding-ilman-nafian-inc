<?php

namespace App\Http\Controllers\Crowfunding;

use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\QueryException;

class ProfileController extends Controller
{
    public function index()
    {
        return view('main.profile');
    }

    public function editProfile()
    {
        return view('main.ubah-profile');
    }

    public function editProfileStore()
    {
        $this->validate(request(), [
            'name' => 'required',
            'gender' => 'required',
        ]);

        try {
            $user = User::where('email', auth()->user()->email)->first();
            $user->update([
                'nama' => request()->name,
                'gender' => request()->gender,
            ]);

            return redirect()->route('donation.success')->with('success-update', 'Update profile success');
        } catch (QueryException $errror) {
            return view('main.errror-page');
        }
    }

    public function editPassword()
    {
        return view('main.ubah-password');
    }

    public function editPasswordStore()
    {
        $this->validate(request(), [
            'oldPass' => 'required',
            'password' => 'required|min:6',
        ]);

        try {
            if (Hash::check(request()->oldPass, auth()->user()->password) == false) {
                return back()->with('fail', ['password' => 'Your old password is wrong']);
            }

            $user = User::where('email', auth()->user()->email)->first();
            $user->update([
                'password' => Hash::make(request()->password)
            ]);

            return redirect()->route('donation.success')->with('success-password', 'Update password success');
        } catch (QueryException $errror) {
            return view('main.errror-page');
        }
    }

    public function addPhoto()
    {
        $this->validate(request(), [
            'photo' => 'required|mimes:png,jpg,jpeg,gif'
        ]);

        try {
            if (request()->file('photo') !== null) {
                if (auth()->user()->photo == null) {
                    $image = base64_encode(file_get_contents(request()->file('photo')->path()));

                    // Obtain the original content (usually binary data)
                    $bin = base64_decode($image);
                    $decoded = base64_decode($image, true);

                    // image verify check
                    if (!is_string($image) || false === $decoded) {
                        return response()->json([
                            'status' => false,
                            'msg' => 'invalid format image'
                        ], 422);
                    }

                    // Load GD resource from binary data
                    $im = imageCreateFromString($bin);

                    if (!$im) {
                        return response()->json([
                            'status' => false,
                            'msg' => 'file is not an image'
                        ], 406);
                    }

                    // Specify the location where you want to save the image
                    $img_name = Str::random(6) . '-' . time() . '.png';
                    if (is_dir(public_path('assets/image/profile/user')) == false) {
                        mkdir(public_path('assets/image/profile/user'));
                    }
                    $img_file = public_path('assets/image/profile/user/' . $img_name);
                    imagepng($im, $img_file, 0);
                    $photo = url('assets/image/profile/user/' . $img_name);

                    $user = User::where('email', auth()->user()->email)->first();
                    $user->update([
                        'photo' => $photo
                    ]);
                } else {
                    // update gambar
                    $current_photo = auth()->user()->photo;
                    $current_photo = explode(url('') . '/', $current_photo);
                    $current_photo = end($current_photo);

                     // delete gambar if file exist
                    if (file_exists(public_path($current_photo)) == true) {
                        unlink($current_photo);
                    }

                    $image = base64_encode(file_get_contents(request()->file('photo')->path()));

                    // Obtain the original content (usually binary data)
                    $bin = base64_decode($image);
                    $decoded = base64_decode($image, true);

                    // image verify check
                    if (!is_string($image) || false === $decoded) {
                        return response()->json([
                            'status' => false,
                            'msg' => 'invalid format image'
                        ], 422);
                    }

                    // Load GD resource from binary data
                    $im = imageCreateFromString($bin);

                    if (!$im) {
                        return response()->json([
                            'status' => false,
                            'msg' => 'file is not an image'
                        ], 406);
                    }

                    // Specify the location where you want to save the image
                    $img_name = Str::random(6) . '-' . time() . '.png';
                    if (is_dir(public_path('assets/image/profile/user')) == false) {
                        mkdir(public_path('assets/image/profile/user'));
                    }
                    $img_file = public_path('assets/image/profile/user/' . $img_name);
                    imagepng($im, $img_file, 0);
                    $photo = url('assets/image/profile/user/' . $img_name);

                    $user = User::where('email', auth()->user()->email)->first();
                    $user->update([
                        'photo' => $photo
                    ]);
                }
            }

            return redirect()->route('donation.success')->with('success-addPhoto', 'Add Photo success');
        } catch (QueryException $errror) {
            return view('main.errror-page');
        }
    }
}

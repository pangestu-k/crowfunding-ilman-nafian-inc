<?php

namespace App\Http\Controllers\Crowfunding;

use App\Models\Donasi;
use App\Models\Kampanye;
use App\Http\Controllers\Controller;

class WelcomeController extends Controller
{
    public function index()
    {
        $donation = Kampanye::orderBy('id','DESC')->get()->map(function($query){
            // rentang hari tanggal
            $diff = strtotime('2023-01-01 06:23:27') - strtotime(now());
            $diff = abs(round($diff / 86400));

            // totol donation
            $donate = Donasi::where('id_kampanye', $query->id)->get()->sum('jumlah');

            // percentage
            $percentage = $donate/$query->target;
            $percentage = round((float)$percentage * 100 ) . '%';


            // change number format
            $donate = number_format($donate);
            $target = number_format($query->target);

            return [
                'id' => $query->id,
                'judul' => $query->judul,
                'deskripsi' => $query->deskripsi,
                'gambar' => $query->gambar,
                'target' => $target,
                'tenggat' => $query->tenggat,
                'status' => $query->status,
                'tenggat_hari' => $diff,
                'total_donasi' => $donate,
                'percentage' => $percentage
            ];
        });

        return view('welcome', compact('donation'));
    }
}

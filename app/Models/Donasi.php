<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Donasi extends Model
{
    use HasFactory;

    protected $table = 'donasi';

    public function campaign()
    {
        return $this->belongsTo(Kampanye::class, 'id_kampanye');
    }
}

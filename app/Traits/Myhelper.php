<?php

namespace App\Traits;

Trait Myhelper{
    public function error_response($error)
    {
        return response()->json([
            'status' => false,
            'msg' => 'something wrong',
            'error' => true,
            'error_detail' => $error
        ], 400);
    }
}

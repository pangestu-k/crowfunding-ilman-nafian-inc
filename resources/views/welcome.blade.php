@extends('layout.layout')

@section('content')
    <div class="container main-content my-4">
        <div class="slider">
            <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="true">
                <div class="carousel-indicators">
                    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active"
                        aria-current="true" aria-label="Slide 1"></button>
                    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1"
                        aria-label="Slide 2"></button>
                    <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2"
                        aria-label="Slide 3"></button>
                </div>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="{{ asset('assets/image/banner/banner-1.png') }}" class="d-block w-100 rounded">
                    </div>
                    <div class="carousel-item">
                        <img src="{{ asset('assets/image/banner/banner-2.png') }}" class="d-block w-100 rounded">
                    </div>
                    <div class="carousel-item">
                        <img src="{{ asset('assets/image/banner/banner-3.png') }}" class="d-block w-100 rounded">
                    </div>
                </div>
            </div>
        </div>

        <br>

        <div class="category row gap-3" style="width: 100% !important; margin-left:2px">

            <a class="col bg-white rounded justify-content-center align-content-center"
                href="{{ route('donation.index') }}">
                <div class="text-center" style="margin-top:13.5px;">
                    <div class="img-category">
                        <img src="{{ asset('assets/image/kategori/quran-3.png') }}" width="40px" height="40px">
                    </div>
                    <div class="text-category">
                        <p>Donasi</p>
                    </div>
                </div>
            </a>

            <a class="col bg-white rounded justify-content-center align-content-center" href="javascript:void(0)"
                onclick="return alert('Fitur masih dalam Pengembangan.')">
                <div class="text-center" style="margin-top:13.5px;">
                    <div class="img-category">
                        <img src="{{ asset('assets/image/kategori/learning.png') }}" width="40px" height="40px">
                    </div>
                    <div class="text-category">
                        <p>Belajar</p>
                    </div>
                </div>
            </a>

            <a class="col bg-white rounded justify-content-center align-content-center" href="javascript:void(0)"
                onclick="return alert('Fitur masih dalam Pengembangan.')">
                <div class="text-center" style="margin-top:13.5px;">
                    <div class="img-category">
                        <img src="{{ asset('assets/image/kategori/bootcamp.png') }}" width="40px" height="40px">
                    </div>
                    <div class="text-category">
                        <p>Pelatihan</p>
                    </div>
                </div>
            </a>

            <a class="col bg-white rounded justify-content-center align-content-center" href="{{ route('about') }}">
                <div class="text-center" style="margin-top:13.5px;">
                    <div class="img-category">
                        <img src="{{ asset('assets/image/kategori/mosque.png') }}" width="40px" height="40px">
                    </div>
                    <div class="text-category">
                        <p>Tentang Kami</p>
                    </div>
                </div>
            </a>
        </div>

        <br>

        <div class="banner-content" style="width: 100% !important">
            @if ($donation->count() !== 0)
                @foreach ($donation as $d)
                    <a href="{{ route('donation.detail', $d['id']) }}">
                        <div class="donation row" style="width: 100% !important; margin-left: 4.5px">
                            <div class="col-5 bg-dark rounded-start campaign-banner-1" style="height: 210px">
                                <img class="image-campagn-banner rounded" src="{{ $d['gambar'] }}">
                            </div>
                            <div class="col bg-dark rounded-end text-white p-3" style="height: 210px">
                                <h5 class="donation-title">{{ $d['judul'] }}</h5>
                                <p style="font-weight: lighter; font-size: 15px; margin-top:20px">Ilman Naafi'an Center</p>
                                <div class="progress" style="margin-top: -10px">
                                    <div class="progress-bar bg-warning" role="progressbar" aria-label="Warning example"
                                        style="width: {{ $d['percentage'] }}" aria-valuenow="75" aria-valuemin="0"
                                        aria-valuemax="100"></div>
                                </div>
                                <div class="amount-info my-1">
                                    <p style="font-size: 15px">
                                        <b>Rp. {{ $d['total_donasi'] }}</b> terkumpul
                                    </p>
                                </div>
                                <div class="expired my-2">
                                    <p>
                                        <i><b>{{ number_format($d['tenggat_hari']) }}</b> hari tersisa</i>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </a>
                @endforeach
            @else
                <div class="donation row text-center" style="width: 100% !important; margin-left: 4.5px">
                    <div class="col bg-dark rounded text-white p-3" style="height: 210px">
                        <h5 class="donation-title">Saat ini kami belum membuka Donai untuk program apapun</h5>
                        <p>Terimakasih Telah mengunjungi website kami, semoga amal kebaikan yang telah diberikan akan
                            dibalas disisinya.</p>
                        <p style="font-weight: lighter; font-size: 15px; margin-top:20px"><i>Ilman Naafi'an Center</i></p>
                    </div>
                </div>
            @endif
            <br>

            {{-- <a href="{{ route('donation.detail') }}">
                <div class="donation row" style="width: 100% !important; margin-left: 4.5px">
                    <div class="col-5 bg-dark rounded-start campaign-banner-1" style="height: 210px">
                        <img class="image-campagn-banner rounded"
                            src="{{ asset('assets/image/campaign/uluran-tangan.jpg') }}">
                    </div>
                    <div class="col bg-dark rounded-end text-white p-3" style="height: 210px">
                        <h5 class="donation-title">Pahala tak terputus, Wakaf Alquran Untuk pondok Tahfidz</h5>
                        <p style="font-weight: lighter; font-size: 15px; margin-top:20px">Ilman Naafi'an Center</p>
                        <div class="progress" style="margin-top: -10px">
                            <div class="progress-bar bg-warning" role="progressbar" aria-label="Warning example"
                                style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <div class="amount-info my-1">
                            <p style="font-size: 15px">
                                <b>Rp. 300,000</b> <i>terkumpul.
                            </p>
                        </div>
                        <div class="expired my-2">
                            <p>
                            <p><b>123</b> hari tersisa</p>
                            </p>
                        </div>
                    </div>
                </div>
            </a>

            <br>

            <a href="{{ route('donation.detail') }}">
                <div class="donation row" style="width: 100% !important; margin-left: 4.5px">
                    <div class="col-5 bg-dark rounded-start campaign-banner-1" style="height: 210px">
                        <img class="image-campagn-banner rounded"
                            src="{{ asset('assets/image/campaign/uluran-tangan.jpg') }}">
                    </div>
                    <div class="col bg-dark rounded-end text-white p-3" style="height: 210px">
                        <h5 class="donation-title">Pahala tak terputus, Wakaf Alquran Untuk pondok Tahfidz</h5>
                        <p style="font-weight: lighter; font-size: 15px; margin-top:20px">Ilman Naafi'an Center</p>
                        <div class="progress" style="margin-top: -10px">
                            <div class="progress-bar bg-warning" role="progressbar" aria-label="Warning example"
                                style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <div class="amount-info my-1">
                            <p style="font-size: 15px">
                                <b>Rp. 300,000</b> <i>terkumpul.
                            </p>
                        </div>
                        <div class="expired my-2">
                            <p>
                            <p><b>123</b> hari tersisa</p>
                            </p>
                        </div>
                    </div>
                </div>
            </a> --}}
        </div>
    </div>
@endsection

@extends('layout.layout')

@section('content')
    <div class="container main-content my-4">
        <div class="banner-content rounded-top about" style="width: 100% !important">

            <div class="col-12  p-3">
                <div class="col-12 text-center bg-warning py-4">
                    <img src="{{ asset('assets/image/logo/logo.jpeg') }}" width="20%" height="20%">
                </div>
            </div>

            <div class="col-12  p-3">
                <div class="col-12 bg-dark text-white p-2" style="text-align: left">
                    <div class="p-2" style="text-align: center">
                        <h4>PROGRAM KOMUNITAS</h4>
                        <h4>EASY TAHFIZH COMMUNITY/ ETC</h4>
                        <h6>(KOMUNITAS KHATAMAN INC)</h6>
                        <h6>Komunitas yang bisa melejitkan diri kamu menjadi manusia langit!</h6>
                    </div>
                    <br>

                    <p>
                        Sebuah proses long life education, yang akan memudahkan Menghafal Al Qur’an sesuai genetika, ETC
                        mencetak pengajar Al Qur’an yang mempunyai hafalan mutqin, yang menciptakan amalan Qur’an dalam
                        kehidupannya sehari-hari.
                    </p>

                    <p>
                        Komunitas ini di inisiasi oleh Ustadzah Rina Setiawati, sebagai wadah pecinta Qur'an untuk konsisten
                        khatam Al Qur'an dengan menghidupkan waktu sepertiga malam dan ba'da maghrib. Juga setoran bacaan
                        harian di grup. Agar kita semakin lancar membaca Al Qur'an, menghafalkan dan mengamalkannya.
                    </p>

                    <p>
                        Ustadzah Rina terpanggil untuk mewujudkan hafizh/ah yang mudah menghafalkan Quran 30 juz dengan
                        rutin membacanya. Karena keluarga dan anak-anak beliau adalah hafizh/ah sejak usia dini.
                    </p>

                    <p>
                        Grup ini juga berisi tentang informasi program sosial, pembelajaran konsep easy tahfizh & metode
                        genetika hijaiyah di INC. Sebuah lembaga Dakwah non profit dengan tag line:
                        JARIYAH BERJAMAAH HIDUPILAH INC, JANGAN MENCARI HIDUP DI INC. Ustadzah Rina, mengambil Prinsip KH.
                        Ahmad Dahlan pendiri Muhammadiyah
                    </p>

                    <p>
                        Komunitas ini juga pecinta yatim piatu dhuafa, yang rutin diberikan donasi saat offline penutupan
                        juz 30.
                    </p>

                    <p style="font-weight: bold">
                        Kegiatannya apa aja di komunitas khataman???
                    </p>

                    <ul style="text-align: left">
                        <li>
                            Ngaji bareng setiap hari di waktu tahajud dan ba'da maghrib, Jam 03.00 - 04.00 & 17.00 - 18.00
                            (luar biasa!)
                        </li>
                        <li>
                            Khataman Akbar : Program membaca Al Qur'an bersama para pengajar INC
                        </li>
                        <li>
                            Silaturahmi offline juz 30 dan kajian QLS (qur'an life series)
                        </li>
                        <li>
                            Wisuda 1000 pengajar Qur'an
                        </li>
                        <li>
                            Pembinaan Pengajaran Qur'an (PPQ)
                        </li>
                        <li>
                            Easy tahfizh (kelas hafalan)
                        </li>
                        <li>
                            Easy tartil (kelas tahsin)
                        </li>
                        <li>
                            Qur'an Life Series : kajian motivasi Al Qur'an yang berkaitan dengan kehidpan
                        </li>
                        <li>
                            Qur'an Solving Series : program konseling untuk menemukan pola hidup yang terarah dengan
                            genetika sesuai Al Qur'an
                        </li>
                        <li>
                            Dompet santunan
                            <ol>
                                <li>
                                    Santunan rutin yatim piatu dhuafa per3 bulan
                                </li>
                                <li>
                                    Santunan akbar yatim dhuafa ramadhan
                                </li>
                            </ol>
                        </li>
                        <li>
                            Program sedekah shubuh
                        </li>
                        <li>
                            Infak terbaik easy tahfizh community
                        </li>
                    </ul>

                    <p>
                        <b>
                            💝🎉Masya Allah Alhamdulillah.... !
                        </b>
                    </p>

                    <p>
                        Ahlan wa Sahlan bagi jamaah di seluruh indonesia, yang sudah bergabung di komunitas khataman☺️🙏🏻
                    </p>

                    <p>
                        🏃🏻‍♂️Ayo sebarin ke banyak grup, keluarga, sahabat, dan teman kita, 1 pekan ajak 1 orang, yang
                        pahalanya akan jadi jariyah abadi
                    </p>

                    <p>
                        Program khatam Qur'an ini sifatnya FREE namun ada Infaq Terbaik bagi yang berkenan
                        (Biasanya kelas tilawah yang kami adakan dengan adanya guru yang mengkoreksi seperti ini bertarifkan
                        dgn
                        biaya 150-600 Rb, bahkan ada yang 1jt per orang karena di lakukan setiap hari 2x sehari)
                    </p>

                    <p>
                        Namun, di INC karena kita punya Tag line <b><i>"JARIYAH BERJAMAAH"</i></b> untuk program rutin ngaji
                        tiap hari ini
                        dengan dibimbing guru, hanya disyaratkan memberikan Infaq Terbaik bagi calon peserta atau siapapun
                        yang
                        sudah lama bergabung
                    </p>

                    <p>
                        <b>
                            😍 Pilihan infaq terbaiknya kami berikan pilihan nominal angka :
                        </b>
                    </p>

                    <ol style="text-align: left">
                        <li style="font-weight: bold">
                            💵 Rp: 50.000
                        </li>
                        <li style="font-weight: bold">
                            💴 Rp: 100.000
                        </li>
                        <li style="font-weight: bold">
                            💶 Rp: 150.000
                        </li>
                        <li style="font-weight: bold">
                            💷 Rp: 200.000
                        </li>
                        <li style="font-weight: bold">
                            💰 Rp: 250.000
                        </li>
                        <li style="font-weight: bold">
                            💎 Tak terbatas dan sesuai dengan kemampuan
                        </li>
                    </ol>

                    <p>
                        Wah gaji tiap hari di koreksi sama guru INC, biayanya murah banget dan bisa milih. Jadi, Silahkan
                        dipilih ya. 😇
                    </p>

                    <div style="text-align: center">
                        <p>
                            Salurkan infaq terbaikmu melalui:
                        </p>

                        <p>
                            <b>💳 Bank Syariah Indonesia (BSI)</b>
                            <br>
                            An Yayasan Al-Qur'an Ilman Naafian
                            <br>
                            <b>779 667 7790</b>
                        </p>

                        <p>
                            <i>
                                atau
                            </i>
                        </p>

                        <p>
                            <b>💳 Bank Syariah Indonesia (BSI)</b>
                            <br>
                            Easy Tahfizh Community
                            <br>
                            An Pembinaan Pengajaran alQuran (PPQ)
                            <br>
                            <b>777 886 8878</b>
                        </p>

                        <p>
                            Konfirmasi :
                            <br>
                            WA Official : <b>0812 8657 024*</b>
                        </p>
                    </div>

                    <p>
                        Insya Allah para pengajar di sini semua berjariyah dan kita juga bisa support ya untuk berikan
                        apresiasi
                        atas jariyah mereka, dari donasi ini kita bisa memberikan berupa 'uang kuota atau bingkisan setiap
                        ada
                        event' sebagai pengganti waktu dan ilmu ilmu beliau yang luar biasa. Meskipun mereka juga tidak
                        mencari
                        hidup di INC dan juga banyak para pengajar yang menghidupi INC serta memberikan dana selain ilmu.
                        Masya
                        Allah tabarokallah 😍 indahnya jariyah berjamaah.
                    </p>

                    <p>
                        Yang jelas infaq yg kamu salurkan, insyaaAllah akan disalurkan untuk segala operasional kelas online
                        seperti zoom, beasiswa untuk peserta, donasi modul gratis & untuk semua pemateri kita nanti ya,
                        karena
                        Allah menjanjikan 10 hingga 700x lipat balasannya lho!
                        <br>
                        <i>"Barang siapa berbuat kebaikan mendapatkan balasan sepuluh kali lipat amalnya...."</i>
                        <b>(QS Al-An'am : 160)</b>
                    </p>

                    <p>
                        <i>
                            "Perumpamaan orang yang menginfakkan hartanya di jalan Allah seperti sebutir biji yang
                            menumbuhkan
                            tujuh
                            tangkai, pada setiap tangkai ada seratus biji. Allah melipatgandakan bagi siapa yg Dia
                            kehendaki,
                            dan
                            Allah Maha Luas, Maha Mengetahui."
                        </i>

                        <br>

                        <b>(QS Al-Baqarah : 261)</b>
                    </p>

                    <p>
                        Berapapun nominalnya, jika ikhlas hanya mengharap ridho Allah, semoga menjadi wasilah kebaikan dan
                        keberkahan hidup kita, aamiin 😇🤭
                        Ayo dukung perjuangan ini dengan jariyah berjamaah 😉
                    </p>

                    <p>
                        <b>Daftar komunitas :</b>
                        <br>
                        isi link : https://bit.ly/ETCommunity
                        <br>
                        e-mail : easytahfizh@gmail.com
                        <br>
                        Wa Official : 0812 8657 024
                    </p>

                    <br>

                    <p style="text-align: center">
                        <b>
                            <i>
                                Tim Ilman Naafi'an Center
                            </i>
                        </b>
                    </p>
                </div>
            </div>

        </div>

        <div class="banner-content rounded-bottom text-center bg-dark px-3 py-4 qris-bg" style="width: 100% !important">
            <a href="{{ asset('assets/image/about/donasi.jpg') }}">
                <img class="img rounded" src="{{ asset('assets/image/about/donasi.jpg') }}" height="45%" width="45%">
            </a>
        </div>
    </div>
@endsection

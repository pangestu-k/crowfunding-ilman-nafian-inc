@extends('layout.layout')

@section('content')
    <div class="container main-content my-4" style="padding-right: 23px">
        <div class="card p-4 rounded">
            <div class="card-header bg-dark text-white">
                Ubah Profil
            </div>

            <div class="card-body">
                @if ($errors->any())
                    <div class="alert bg-danger alert-dismissible fade show" role="alert">
                        <ul style="text-align: left">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif

                @if (Session::get('fail') !== null)
                    <div class="alert bg-danger alert-dismissible fade show" role="alert">
                        <ul style="text-align: left">
                            @foreach (Session::get('fail') as $key => $value)
                                <li>{{ $value }}</li>
                            @endforeach
                        </ul>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                <form action="{{ route('editProfile.store') }}" method="POST">
                    @csrf
                    <div class="input-group mb-3">
                        <span class="input-group-text">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                class="bi bi-person" viewBox="0 0 16 16">
                                <path
                                    d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6Zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0Zm4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4Zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10Z" />
                            </svg>
                        </span>
                        <input class="form-control" type="text" id="name" name="name" placeholder="Nama lengkap"
                            value="{{ old('name', auth()->user()->nama) }}" autocomplete="off">
                    </div>
                    <div class="mb-3 rounded p-2" style="text-align: left;background: whitesmoke">
                        <div class="form-check">
                            <input class="form-check-input gender-select" type="radio" value="laki-laki"
                                name="gender" id="gender1" {{ auth()->user()->gender == 'laki-laki' ? 'checked' : '' }}>
                            <label class="form-check-label gender-label" for="gender1">
                                Laki-Laki
                            </label>
                        </div>

                        <div class="form-check">
                            <input class="form-check-input gender-select" type="radio" value="perempuan"
                                name="gender" id="gender2" {{ auth()->user()->gender == 'perempuan' ? 'checked' : '' }}>
                            <label class="form-check-label gender-label" for="gender2">
                                Perempuan
                            </label>
                        </div>
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-warning rounded-pill" style="width: 80%">Selesai</button>
                    </div>
                </form>
            </div>
        </div>
    @endsection

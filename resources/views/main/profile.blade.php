@extends('layout.layout')

@section('content')
    @auth
        <div class="container main-content my-4" style="padding-right: 23px">
            <br><br><br>
            <div class="banner-content" style="width: 100% !important">
                <div class="profile row" style="width: 100% !important; margin-left: 4.5px">
                    <div class="col-12 profie-banner rounded text-white text-center p-4"
                        style="height: 220px;background: black;position: relative;">
                        @if (auth()->user()->gender == 'laki-laki')
                            <img src="{{ auth()->user()->photo !== null ? auth()->user()->photo : asset('assets/image/profile/muslim.png') }}"
                                class="img avatar-thumbnail rounded-circle" style="object-fit: cover;" alt=""
                                width="200px" height="200px">
                        @else
                            <img src="{{ auth()->user()->photo !== null ? auth()->user()->photo : asset('assets/image/profile/muslimah.png') }}"
                                class="img avatar-thumbnail rounded-circle" style="object-fit: cover;" alt=""
                                width="200px" height="200px">
                        @endif
                        <p class="text-dark edit-text bg-warning" id="edit-text">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                class="bi bi-pencil-square" viewBox="0 0 16 16">
                                <path
                                    d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z" />
                                <path fill-rule="evenodd"
                                    d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z" />
                            </svg>
                        </p>
                        <h2 style="margin-top: 25px; color:whitesmoke" class="font-weight-bold">{{ auth()->user()->nama }}</h2>
                    </div>
                </div>
            </div>
            <br>

            <div class="banner-content text-center" style="width: 100% !important">
                <a href="{{ route('donation.user', auth()->user()->id) }}">
                    <div class="donation row bg-dark rounded text-white" style="width: 100% !important; margin-left: 4.5px;">
                        <div class="col-3" style="height: 80px; line-height: 60px">
                            <div class="bg-warning rounded" style="height: 60px; width: 60px; margin-top:10px">
                                <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="black"
                                    class="bi bi-cash-coin" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd"
                                        d="M11 15a4 4 0 1 0 0-8 4 4 0 0 0 0 8zm5-4a5 5 0 1 1-10 0 5 5 0 0 1 10 0z" />
                                    <path
                                        d="M9.438 11.944c.047.596.518 1.06 1.363 1.116v.44h.375v-.443c.875-.061 1.386-.529 1.386-1.207 0-.618-.39-.936-1.09-1.1l-.296-.07v-1.2c.376.043.614.248.671.532h.658c-.047-.575-.54-1.024-1.329-1.073V8.5h-.375v.45c-.747.073-1.255.522-1.255 1.158 0 .562.378.92 1.007 1.066l.248.061v1.272c-.384-.058-.639-.27-.696-.563h-.668zm1.36-1.354c-.369-.085-.569-.26-.569-.522 0-.294.216-.514.572-.578v1.1h-.003zm.432.746c.449.104.655.272.655.569 0 .339-.257.571-.709.614v-1.195l.054.012z" />
                                    <path
                                        d="M1 0a1 1 0 0 0-1 1v8a1 1 0 0 0 1 1h4.083c.058-.344.145-.678.258-1H3a2 2 0 0 0-2-2V3a2 2 0 0 0 2-2h10a2 2 0 0 0 2 2v3.528c.38.34.717.728 1 1.154V1a1 1 0 0 0-1-1H1z" />
                                    <path d="M9.998 5.083 10 5a2 2 0 1 0-3.132 1.65 5.982 5.982 0 0 1 3.13-1.567z" />
                                </svg>
                            </div>
                        </div>
                        <div style="height: 80px; line-height: 80px;font-size:20px" class="col-6">
                            Donasi Saya
                        </div>
                        <div style="height: 80px; line-height: 80px" class="col-3">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                class="bi bi-arrow-right arrow-profile" viewBox="0 0 16 16">
                                <path fill-rule="evenodd"
                                    d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z" />
                            </svg>
                        </div>
                    </div>
                </a>

                <br>
                <a href="{{ route('editProfile.get') }}">
                    <div class="donation row bg-dark rounded text-white" style="width: 100% !important; margin-left: 4.5px;">
                        <div class="col-3" style="height: 80px; line-height: 60px">
                            <div class="bg-warning rounded" style="height: 60px; width: 60px; margin-top:10px">
                                <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="black"
                                    class="bi bi-person-fill" viewBox="0 0 16 16">
                                    <path d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z" />
                                </svg>
                            </div>
                        </div>
                        <div style="height: 80px; line-height: 80px;font-size:20px" class="col-6">
                            Ubah Profile
                        </div>
                        <div style="height: 80px; line-height: 80px" class="col-3">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                class="bi bi-arrow-right arrow-profile" viewBox="0 0 16 16">
                                <path fill-rule="evenodd"
                                    d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z" />
                            </svg>
                        </div>
                    </div>
                </a>

                <br>
                <a href="{{ route('editPassword.get') }}">
                    <div class="donation row bg-dark rounded text-white" style="width: 100% !important; margin-left: 4.5px;">
                        <div class="col-3" style="height: 80px; line-height: 60px">
                            <div class="bg-warning rounded" style="height: 60px; width: 60px; margin-top:10px">
                                <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="black"
                                    class="bi bi-key-fill" viewBox="0 0 16 16">
                                    <path
                                        d="M3.5 11.5a3.5 3.5 0 1 1 3.163-5H14L15.5 8 14 9.5l-1-1-1 1-1-1-1 1-1-1-1 1H6.663a3.5 3.5 0 0 1-3.163 2zM2.5 9a1 1 0 1 0 0-2 1 1 0 0 0 0 2z" />
                                </svg>
                            </div>
                        </div>
                        <div style="height: 80px; line-height: 80px;font-size:20px" class="col-6">
                            Ubah Password
                        </div>
                        <div style="height: 80px; line-height: 80px" class="col-3">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                class="bi bi-arrow-right arrow-profile" viewBox="0 0 16 16">
                                <path fill-rule="evenodd"
                                    d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z" />
                            </svg>
                        </div>
                    </div>
                </a>

                <br>

                <a href="javascript:void(0)" id="a-submit">
                    <div class="donation row bg-dark rounded-pill text-white"
                        style="width: 100% !important; margin-left: 4.5px;">
                        <div class="col rounded-pill text-white"
                            style="height: 55px; line-height: 55px; background:maroon; font-size: 20px">
                            Logout
                            <svg xmlns="http://www.w3.org/2000/svg" style="margin-left: 10px" width="25" height="25"
                                fill="white" class="bi bi-box-arrow-left" viewBox="0 0 16 16">
                                <path fill-rule="evenodd"
                                    d="M6 12.5a.5.5 0 0 0 .5.5h8a.5.5 0 0 0 .5-.5v-9a.5.5 0 0 0-.5-.5h-8a.5.5 0 0 0-.5.5v2a.5.5 0 0 1-1 0v-2A1.5 1.5 0 0 1 6.5 2h8A1.5 1.5 0 0 1 16 3.5v9a1.5 1.5 0 0 1-1.5 1.5h-8A1.5 1.5 0 0 1 5 12.5v-2a.5.5 0 0 1 1 0v2z" />
                                <path fill-rule="evenodd"
                                    d="M.146 8.354a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L1.707 7.5H10.5a.5.5 0 0 1 0 1H1.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3z" />
                            </svg>
                        </div>
                    </div>
                </a>

                <form action="{{ route('user.logout') }}" method="POST" id="form-logout"
                    onsubmit="return confirm('Anda Yakin Ingin Keluar ?')">
                    @csrf
                    <button type="submit" id="b-submit" class="d-none"></button>
                </form>
            </div>
        </div>
        <button type="button" id="modal-upload" class="btn btn-warning rounded-pill d-none" data-bs-toggle="modal"
            data-bs-target="#upload" data-bs-whatever="@mdo">Upload Photo</button>

        <div class="modal fade" id="upload" tabindex="-1" aria-labelledby="uploadLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-dark text-white">
                        <h5 class="modal-title" id="exampleModalLabel">Upload Foto Profile</h5>
                        <button type="button" class="btn-light" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        @if ($errors->any())
                            <div class="alert bg-danger alert-dismissible fade show" role="alert">
                                <ul style="text-align: left">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                                <button type="button" class="btn-close" data-bs-dismiss="alert"
                                    aria-label="Close"></button>
                            </div>
                        @endif
                        <form action="{{ route('addPhoto.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="file-upload">
                                <button class="file-upload-btn" type="button"
                                    onclick="$('.file-upload-input').trigger( 'click' )">Pilih
                                    Gambar</button>

                                <div class="image-upload-wrap">
                                    <input class="file-upload-input" required type='file' id="upload-image"
                                        onchange="readURL(this);" accept="image/*" name="photo" />
                                    <div class="drag-text">
                                        <h3>Drag & drop File Gambar yang Akan Dikirimkan</h3>
                                    </div>
                                </div>
                                <div class="file-upload-content">
                                    <img class="file-upload-image" src="#" alt="your image" />
                                    <div class="image-title-wrap">
                                        <button type="button" onclick="removeUpload()" class="remove-image">Remove <span
                                                class="image-title">Uploaded Image</span></button>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer bg-dark">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-warning">Kirim</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @push('additional-js')
            <script>
                document.getElementById("a-submit").addEventListener("click", function(event) {
                    event.preventDefault()
                    document.getElementById('b-submit').click();
                });

                document.getElementById("edit-text").addEventListener("click", function(event) {
                    document.getElementById('modal-upload').click();
                });

                function readURL(input) {
                    if (input.files && input.files[0]) {

                        var reader = new FileReader();

                        reader.onload = function(e) {
                            $('.image-upload-wrap').hide();

                            $('.file-upload-image').attr('src', e.target.result);
                            $('.file-upload-content').show();

                            $('.image-title').html(input.files[0].name);
                        };

                        reader.readAsDataURL(input.files[0]);

                    } else {
                        removeUpload();
                    }
                }

                function removeUpload() {
                    $('.file-upload-input').replaceWith($('.file-upload-input').clone());
                    $('.file-upload-content').hide();
                    $('.image-upload-wrap').show();
                    $('#upload-image').val('')
                }
                $('.image-upload-wrap').bind('dragover', function() {
                    $('.image-upload-wrap').addClass('image-dropping');
                });
                $('.image-upload-wrap').bind('dragleave', function() {
                    $('.image-upload-wrap').removeClass('image-dropping');
                });
            </script>
        @endpush
    @endauth

    @guest
        <div class="container main-content my-4">
            <div class="card p-4 rounded">
                <div class="card-header bg-dark text-white">
                    @if (isset($_GET['register']))
                        Register
                    @else
                        Login
                    @endif
                </div>

                <div class="card-body text-center">
                    @if ($errors->any())
                        <div class="alert bg-danger alert-dismissible fade show" role="alert">
                            <ul style="text-align: left">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif

                    @if (Session::get('fail') !== null)
                        <div class="alert bg-danger alert-dismissible fade show" role="alert">
                            <ul style="text-align: left">
                                @foreach (Session::get('fail') as $key => $value)
                                    <li>{{ $value }}</li>
                                @endforeach
                            </ul>
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif

                    @if (isset($_GET['register']))
                        <form action="{{ route('user.register') }}" method="POST">
                            @csrf
                            <div class="input-group mb-3">
                                <span class="input-group-text">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                        fill="currentColor" class="bi bi-person" viewBox="0 0 16 16">
                                        <path
                                            d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6Zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0Zm4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4Zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10Z" />
                                    </svg>
                                </span>
                                <input class="form-control" type="text" id="name" name="name"
                                    placeholder="Nama lengkap" value="{{ old('name') }}" autocomplete="off">
                            </div>
                            <div class="input-group mb-3">
                                <span class="input-group-text">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                        fill="currentColor" class="bi bi-envelope-paper" viewBox="0 0 16 16">
                                        <path
                                            d="M4 0a2 2 0 0 0-2 2v1.133l-.941.502A2 2 0 0 0 0 5.4V14a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V5.4a2 2 0 0 0-1.059-1.765L14 3.133V2a2 2 0 0 0-2-2H4Zm10 4.267.47.25A1 1 0 0 1 15 5.4v.817l-1 .6v-2.55Zm-1 3.15-3.75 2.25L8 8.917l-1.25.75L3 7.417V2a1 1 0 0 1 1-1h8a1 1 0 0 1 1 1v5.417Zm-11-.6-1-.6V5.4a1 1 0 0 1 .53-.882L2 4.267v2.55Zm13 .566v5.734l-4.778-2.867L15 7.383Zm-.035 6.88A1 1 0 0 1 14 15H2a1 1 0 0 1-.965-.738L8 10.083l6.965 4.18ZM1 13.116V7.383l4.778 2.867L1 13.117Z" />
                                    </svg>
                                </span>
                                <input class="form-control" type="email" id="email" name="email"
                                    placeholder="Example@gmail.com" value="{{ old('email') }}" autocomplete="off">
                            </div>

                            <div class="mb-3 rounded p-2" style="text-align: left;background: whitesmoke">
                                <div class="form-check">
                                    <input class="form-check-input gender-select" type="radio" value="laki-laki"
                                        name="gender" id="gender1">
                                    <label class="form-check-label gender-label" for="gender1">
                                        Laki-Laki
                                    </label>
                                </div>

                                <div class="form-check">
                                    <input class="form-check-input gender-select" type="radio" value="perempuan"
                                        name="gender" id="gender2">
                                    <label class="form-check-label gender-label" for="gender2">
                                        Perempuan
                                    </label>
                                </div>
                            </div>

                            <div class="input-group mb-3">
                                <span class="input-group-text">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20"
                                        fill="currentColor" class="bi bi-file-lock" viewBox="0 0 16 16">
                                        <path
                                            d="M8 5a1 1 0 0 1 1 1v1H7V6a1 1 0 0 1 1-1zm2 2.076V6a2 2 0 1 0-4 0v1.076c-.54.166-1 .597-1 1.224v2.4c0 .816.781 1.3 1.5 1.3h3c.719 0 1.5-.484 1.5-1.3V8.3c0-.627-.46-1.058-1-1.224zM6.105 8.125A.637.637 0 0 1 6.5 8h3a.64.64 0 0 1 .395.125c.085.068.105.133.105.175v2.4c0 .042-.02.107-.105.175A.637.637 0 0 1 9.5 11h-3a.637.637 0 0 1-.395-.125C6.02 10.807 6 10.742 6 10.7V8.3c0-.042.02-.107.105-.175z" />
                                        <path
                                            d="M4 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H4zm0 1h8a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1z" />
                                    </svg>
                                </span>
                                <input class="form-control" type="password" id="password" name="password"
                                    placeholder="Password">
                                <span class="input-group-text">
                                    <i class="fa fa-eye" id="togglePassword" style="cursor: pointer">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20"
                                            fill="currentColor" id="svg-eye" class="bi bi-eye" viewBox="0 0 16 16">
                                            <path
                                                d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.133 13.133 0 0 1 1.66-2.043C4.12 4.668 5.88 3.5 8 3.5c2.12 0 3.879 1.168 5.168 2.457A13.133 13.133 0 0 1 14.828 8c-.058.087-.122.183-.195.288-.335.48-.83 1.12-1.465 1.755C11.879 11.332 10.119 12.5 8 12.5c-2.12 0-3.879-1.168-5.168-2.457A13.134 13.134 0 0 1 1.172 8z" />
                                            <path
                                                d="M8 5.5a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5zM4.5 8a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0z" />
                                        </svg>
                                    </i>
                                </span>
                            </div>
                            <div class="input-group mb-3">
                                <span class="input-group-text">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20"
                                        fill="currentColor" class="bi bi-file-lock" viewBox="0 0 16 16">
                                        <path
                                            d="M8 5a1 1 0 0 1 1 1v1H7V6a1 1 0 0 1 1-1zm2 2.076V6a2 2 0 1 0-4 0v1.076c-.54.166-1 .597-1 1.224v2.4c0 .816.781 1.3 1.5 1.3h3c.719 0 1.5-.484 1.5-1.3V8.3c0-.627-.46-1.058-1-1.224zM6.105 8.125A.637.637 0 0 1 6.5 8h3a.64.64 0 0 1 .395.125c.085.068.105.133.105.175v2.4c0 .042-.02.107-.105.175A.637.637 0 0 1 9.5 11h-3a.637.637 0 0 1-.395-.125C6.02 10.807 6 10.742 6 10.7V8.3c0-.042.02-.107.105-.175z" />
                                        <path
                                            d="M4 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H4zm0 1h8a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1z" />
                                    </svg>
                                </span>
                                <input class="form-control" type="password" id="password-confirmation"
                                    name="password_confirmation" placeholder="Konfirmasi Password">
                                <span class="input-group-text">
                                    <i class="fa fa-eye" id="togglePassword-confirm" style="cursor: pointer">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20"
                                            fill="currentColor" id="svg-eye" class="bi bi-eye" viewBox="0 0 16 16">
                                            <path
                                                d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.133 13.133 0 0 1 1.66-2.043C4.12 4.668 5.88 3.5 8 3.5c2.12 0 3.879 1.168 5.168 2.457A13.133 13.133 0 0 1 14.828 8c-.058.087-.122.183-.195.288-.335.48-.83 1.12-1.465 1.755C11.879 11.332 10.119 12.5 8 12.5c-2.12 0-3.879-1.168-5.168-2.457A13.134 13.134 0 0 1 1.172 8z" />
                                            <path
                                                d="M8 5.5a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5zM4.5 8a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0z" />
                                        </svg>
                                    </i>
                                </span>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-warning rounded-pill"
                                    style="width: 80%">Register</button>
                            </div>
                        </form>
                    @else
                        <form action="{{ route('user.login') }}" method="POST">
                            @csrf
                            <div class="input-group mb-3">
                                <span class="input-group-text">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                        fill="currentColor" class="bi bi-envelope-paper" viewBox="0 0 16 16">
                                        <path
                                            d="M4 0a2 2 0 0 0-2 2v1.133l-.941.502A2 2 0 0 0 0 5.4V14a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V5.4a2 2 0 0 0-1.059-1.765L14 3.133V2a2 2 0 0 0-2-2H4Zm10 4.267.47.25A1 1 0 0 1 15 5.4v.817l-1 .6v-2.55Zm-1 3.15-3.75 2.25L8 8.917l-1.25.75L3 7.417V2a1 1 0 0 1 1-1h8a1 1 0 0 1 1 1v5.417Zm-11-.6-1-.6V5.4a1 1 0 0 1 .53-.882L2 4.267v2.55Zm13 .566v5.734l-4.778-2.867L15 7.383Zm-.035 6.88A1 1 0 0 1 14 15H2a1 1 0 0 1-.965-.738L8 10.083l6.965 4.18ZM1 13.116V7.383l4.778 2.867L1 13.117Z" />
                                    </svg>
                                </span>
                                <input class="form-control" type="email" id="email" name="email"
                                    value="{{ Session::get('email') !== null ? Session::get('email') : '' }}"
                                    placeholder="example@gmail.com">
                            </div>
                            <div class="input-group mb-3">
                                <span class="input-group-text">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20"
                                        fill="currentColor" class="bi bi-file-lock" viewBox="0 0 16 16">
                                        <path
                                            d="M8 5a1 1 0 0 1 1 1v1H7V6a1 1 0 0 1 1-1zm2 2.076V6a2 2 0 1 0-4 0v1.076c-.54.166-1 .597-1 1.224v2.4c0 .816.781 1.3 1.5 1.3h3c.719 0 1.5-.484 1.5-1.3V8.3c0-.627-.46-1.058-1-1.224zM6.105 8.125A.637.637 0 0 1 6.5 8h3a.64.64 0 0 1 .395.125c.085.068.105.133.105.175v2.4c0 .042-.02.107-.105.175A.637.637 0 0 1 9.5 11h-3a.637.637 0 0 1-.395-.125C6.02 10.807 6 10.742 6 10.7V8.3c0-.042.02-.107.105-.175z" />
                                        <path
                                            d="M4 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H4zm0 1h8a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1z" />
                                    </svg>
                                </span>
                                <input class="form-control" type="password" id="password" name="password"
                                    placeholder="Password">
                                <span class="input-group-text">
                                    <i class="fa fa-eye" id="togglePassword" style="cursor: pointer">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20"
                                            fill="currentColor" id="svg-eye" class="bi bi-eye" viewBox="0 0 16 16">
                                            <path
                                                d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.133 13.133 0 0 1 1.66-2.043C4.12 4.668 5.88 3.5 8 3.5c2.12 0 3.879 1.168 5.168 2.457A13.133 13.133 0 0 1 14.828 8c-.058.087-.122.183-.195.288-.335.48-.83 1.12-1.465 1.755C11.879 11.332 10.119 12.5 8 12.5c-2.12 0-3.879-1.168-5.168-2.457A13.134 13.134 0 0 1 1.172 8z" />
                                            <path
                                                d="M8 5.5a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5zM4.5 8a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0z" />
                                        </svg>
                                    </i>
                                </span>
                            </div>

                            <div class="text-center">
                                <button type="submit" class="btn btn-warning rounded-pill" style="width: 80%">Login</button>
                            </div>
                        </form>
                    @endif

                </div>
                <div class="card-footer text-center" style="background: white !important; border:none">
                    @if (isset($_GET['register']))
                        <a href="{{ route('profile') }}" class="text-secondary ">
                            <i>
                                Sudah Punya Akun ? Login Disini
                            </i>
                        </a>
                    @else
                        <a href="{{ route('profile') }}?register=true" class="text-secondary ">
                            <i>
                                Belum Punya Akun ? Daftar Disini
                            </i>
                        </a>
                    @endif

                </div>
            </div>
        </div>

        @push('additional-js')
            <script>
                const togglePassword = document.querySelector("#togglePassword");
                const password = document.querySelector("#password");

                togglePassword.addEventListener("click", function() {

                    // toggle the type attribute
                    const type = password.getAttribute("type") === "password" ? "text" : "password";
                    password.setAttribute("type", type);
                    // toggle the eye icon
                    document.querySelector("#svg-eye").classList('bi bi-eye');
                    document.querySelector("#svg-eye").classList('bi bi-eye-slash');

                });

                const togglePasswordConfirm = document.querySelector("#togglePassword-confirm");
                const passwordConfirm = document.querySelector("#password-confirmation");

                togglePasswordConfirm.addEventListener("click", function() {

                    // toggle the type attribute
                    const type = passwordConfirm.getAttribute("type") === "password" ? "text" : "password";
                    passwordConfirm.setAttribute("type", type);
                    // toggle the eye icon
                    document.querySelector("#svg-eye").classList('bi bi-eye');
                    document.querySelector("#svg-eye").classList('bi bi-eye-slash');

                });
            </script>
        @endpush
    @endguest
@endsection

@extends('layout.layout')

@section('content')
    <div class="container main-content my-4" style="padding-right: 23px">
        <div class="card p-4 rounded">
            <div class="card-header bg-dark text-white">
                Form Donasi
            </div>

            <div class="card-body">
                <form action="{{ route('donation.success') }}">
                    <div class="input-group mb-3">
                        <span class="input-group-text">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                class="bi bi-wallet2" viewBox="0 0 16 16">
                                <path
                                    d="M12.136.326A1.5 1.5 0 0 1 14 1.78V3h.5A1.5 1.5 0 0 1 16 4.5v9a1.5 1.5 0 0 1-1.5 1.5h-13A1.5 1.5 0 0 1 0 13.5v-9a1.5 1.5 0 0 1 1.432-1.499L12.136.326zM5.562 3H13V1.78a.5.5 0 0 0-.621-.484L5.562 3zM1.5 4a.5.5 0 0 0-.5.5v9a.5.5 0 0 0 .5.5h13a.5.5 0 0 0 .5-.5v-9a.5.5 0 0 0-.5-.5h-13z" />
                            </svg>
                        </span>
                        <input class="form-control number-separator" type="text" id="donate" name="donate"
                            placeholder="Masukan Jumlah Donasi" required>
                    </div>

                    <textarea name="keterangan" class="form-control" id="keterangan" rows="10" placeholder="Keterangan / Do'a dari Donatur"></textarea>


                    <br>

                    <div class="text-center">
                        <button type="submit" class="btn btn-warning rounded-pill" style="width: 80%">Kirimkan</button>
                    </div>
                </form>
            </div>

        </div>
    </div>

    @push('additional-js')
        <script>
            easyNumberSeparator({
                selector: '.number-separator',
            })

            easyNumberSeparator({
                selector: '.number-separator',
                separator: ','
            })

            ClassicEditor
                .create(document.querySelector('#keterangan'))
                .catch(error => {
                    console.error(error);
                });
        </script>
    @endpush
@endsection

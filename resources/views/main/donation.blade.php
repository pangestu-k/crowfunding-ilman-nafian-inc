@extends('layout.layout')

@section('content')
<div class="container main-content my-4">
    <a href="#">
       <div class="donation row" style="width: 100% !important; margin-left: 4.5px">
        <div class="col-5 bg-dark rounded-start campaign-banner-1" style="height: 210px">
            <img class="image-campagn-banner rounded" src="{{ asset('assets/image/campaign/uluran-tangan.jpg') }}">
        </div>
        <div class="col bg-dark rounded-end text-white p-3" style="height: 210px">
            <h5 class="donation-title">Pahala tak terputus, Wakaf Alquran Untuk pondok Tahfidz</h5>
            <p style="font-weight: lighter; font-size: 15px; margin-top:20px">Ilman Naafi'an Center</p>
            <div class="progress" style="margin-top: -10px">
                <div class="progress-bar bg-warning" role="progressbar" aria-label="Warning example"
                    style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <div class="amount-info my-1">
                <p style="font-size: 15px">
                    <b>Rp. 300,000</b> <i>terkumpul.
                </p>
            </div>
            <div class="expired my-2">
                <p>
                    <p><b>123</b> hari tersisa</p>
                </p>
            </div>
        </div>
    </div>
    </a>


    <br>

    <div class="donation row" style="width: 100% !important; margin-left: 4.5px">
        <div class="col-5 bg-dark rounded-start campaign-banner-1" style="height: 210px">
            <img class="image-campagn-banner rounded" src="{{ asset('assets/image/campaign/uluran-tangan.jpg') }}">
        </div>
        <div class="col bg-dark rounded-end text-white p-3" style="height: 210px">
            <h5 class="donation-title">Pahala tak terputus, Wakaf Alquran Untuk pondok Tahfidz</h5>
            <p style="font-weight: lighter; font-size: 15px; margin-top:20px">Ilman Naafi'an Center</p>
            <div class="progress" style="margin-top: -10px">
                <div class="progress-bar bg-warning" role="progressbar" aria-label="Warning example"
                    style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <div class="amount-info my-1">
                <p style="font-size: 15px">
                    <b>Rp. 300,000</b> <i>terkumpul.
                </p>
            </div>
            <div class="expired my-2">
                <p>
                    <p><b>123</b> hari tersisa</p>
                </p>
            </div>
        </div>
    </div>

    <br>

    <div class="donation row" style="width: 100% !important; margin-left: 4.5px">
        <div class="col-5 bg-dark rounded-start campaign-banner-1" style="height: 210px">
            <img class="image-campagn-banner rounded" src="{{ asset('assets/image/campaign/uluran-tangan.jpg') }}">
        </div>
        <div class="col bg-dark rounded-end text-white p-3" style="height: 210px">
            <h5 class="donation-title">Pahala tak terputus, Wakaf Alquran Untuk pondok Tahfidz</h5>
            <p style="font-weight: lighter; font-size: 15px; margin-top:20px">Ilman Naafi'an Center</p>
            <div class="progress" style="margin-top: -10px">
                <div class="progress-bar bg-warning" role="progressbar" aria-label="Warning example"
                    style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <div class="amount-info my-1">
                <p style="font-size: 15px">
                    <b>Rp. 300,000</b> <i>terkumpul.
                </p>
            </div>
            <div class="expired my-2">
                <p>
                    <p><b>123</b> hari tersisa</p>
                </p>
            </div>
        </div>
    </div>

    <br>

    <div class="donation row" style="width: 100% !important; margin-left: 4.5px">
        <div class="col-5 bg-dark rounded-start campaign-banner-1" style="height: 210px">
            <img class="image-campagn-banner rounded" src="{{ asset('assets/image/campaign/uluran-tangan.jpg') }}">
        </div>
        <div class="col bg-dark rounded-end text-white p-3" style="height: 210px">
            <h5 class="donation-title">Pahala tak terputus, Wakaf Alquran Untuk pondok Tahfidz</h5>
            <p style="font-weight: lighter; font-size: 15px; margin-top:20px">Ilman Naafi'an Center</p>
            <div class="progress" style="margin-top: -10px">
                <div class="progress-bar bg-warning" role="progressbar" aria-label="Warning example"
                    style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <div class="amount-info my-1">
                <p style="font-size: 15px">
                    <b>Rp. 300,000</b> <i>terkumpul.
                </p>
            </div>
            <div class="expired my-2">
                <p>
                    <p><b>123</b> hari tersisa</p>
                </p>
            </div>
        </div>
    </div>

    <br>

    <div class="donation row" style="width: 100% !important; margin-left: 4.5px">
        <div class="col-5 bg-dark rounded-start campaign-banner-1" style="height: 210px">
            <img class="image-campagn-banner rounded" src="{{ asset('assets/image/campaign/uluran-tangan.jpg') }}">
        </div>
        <div class="col bg-dark rounded-end text-white p-3" style="height: 210px">
            <h5 class="donation-title">Pahala tak terputus, Wakaf Alquran Untuk pondok Tahfidz</h5>
            <p style="font-weight: lighter; font-size: 15px; margin-top:20px">Ilman Naafi'an Center</p>
            <div class="progress" style="margin-top: -10px">
                <div class="progress-bar bg-warning" role="progressbar" aria-label="Warning example"
                    style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <div class="amount-info my-1">
                <p style="font-size: 15px">
                    <b>Rp. 300,000</b> <i>terkumpul.
                </p>
            </div>
            <div class="expired my-2">
                <p>
                    <p><b>123</b> hari tersisa</p>
                </p>
            </div>
        </div>
    </div>
</div>
@endsection

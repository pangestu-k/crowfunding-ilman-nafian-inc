@extends('layout.layout')

@section('content')
    <div class="container main-content my-4" style="padding-right: 23px">
        <div class="card p-4 rounded">
            <div class="card-body" style="text-align: center">
                <img src="{{ asset('assets/image/icon/success.gif') }}" width="80%" alt="">
                <h1>Success</h1>
                @if (Session::get('success-password') !== null)
                    <p>{{ Session::get('success-password') }}</p>
                @elseif (Session::get('success-update') !== null)
                    <p>{{ Session::get('success-update') }}</p>
                @elseif (Session::get('success-addPhoto') !== null)
                    <p>{{ Session::get('success-addPhoto') }}</p>
                @else
                    <p>Silahkan Konfirmasi Pembayaran Donasi</p>
                @endif
                <div class="btn btn-warning text-dark rounded-pill">Halaman Akan Otomatis Tertutup Dalam <b><span
                            id="time">5</span> <i>detik</i></b></div>
            </div>
        </div>
    </div>

    @if (Session::get('success-password') !== null || Session::get('success-update') !== null || Session::get('success-addPhoto') !== null)
        <script>
            var url = "{{ url(route('profile')) }}"
            console.log(url);

            function startTimer(duration, display) {
                var timer = duration,
                    seconds;
                setInterval(function() {
                    seconds = parseInt(timer % 60, 10);

                    display.textContent = seconds;

                    if (--timer < 0) {
                        display.textContent = 0;
                        setInterval(function() {
                            window.location.replace(url)
                        }, 2000)
                    }
                }, 1000);
            }

            window.onload = function() {
                var fiveSecond = 5,
                    display = document.querySelector('#time');
                startTimer(fiveSecond, display);
            };
        </script>
    @else
        <script>
            var url = "{{ url(route('donation.user')) }}"
            console.log(url);

            function startTimer(duration, display) {
                var timer = duration,
                    seconds;
                setInterval(function() {
                    seconds = parseInt(timer % 60, 10);

                    display.textContent = seconds;

                    if (--timer < 0) {
                        display.textContent = 0;
                        setInterval(function() {
                            window.location.replace(url)
                        }, 2000)
                    }
                }, 1000);
            }

            window.onload = function() {
                var fiveSecond = 5,
                    display = document.querySelector('#time');
                startTimer(fiveSecond, display);
            };
        </script>
    @endif
@endsection
